var express = require('express');
var morgan = require('morgan');
var path = require('path');
var app = express();
var config = require('./config');

// events
var events = require('events');
var emitter = new events.EventEmitter();
// socket stuff
var users = [];
var socket = require('socket.io');
var io = socket(app.listen(9091));
io.on('connection', (client) => {
    var id = client.id;
    console.log('socket client connected to server');
    client.on('new-user', function (data) {
        users.push({
            id: id,
            name: data
        });
        client.emit('users', users);
        client.broadcast.emit('users', users);
    });
    client.on('new-msg', function (data) {
        console.log('new msg', data);
        client.emit('reply-msg', data); //emit garne connected client
        client.broadcast.to(data.receiverId).emit('reply-msg-user', data); // connected client bahek baki client
    })

    client.on('disconnect', function () {

        console.log('client disconnected when logout');
        users.forEach(function (data, i) {
            console.log('data .id >>', data.id);
            if (data.id == id) {
                users.splice(i, 1);
            }
            client.emit('users', users);
            client.broadcast.emit('users', users);
        })
    })

});

require('./db');
// load routing level middleware
var api = require("./controllers/api.route")('emitter')

// load thrid party middleware
app.use(morgan('dev'));

// inbuilt middleware
app.use('/file', express.static(path.join(__dirname, 'uploads'))); // server static file externally
app.use(express.urlencoded({ extended: true }))
app.use(express.json());


// 
var cors = require('cors');
app.use(cors());
app.use(function (req, res, next) {
    req.emitter = emitter;
    next();
})
app.use('/api', api);

app.use(function (req, res, next) {
    // 404 error handler
    // validate ticket
    console.log('i am applicaiton level middleware ');
    next({
        msg: 'Not Found',
        status: 404
    });
});

emitter.on('hello', function (data) {
    console.log('data in hello', data);
})

// error handling middleware
app.use(function (err, req, res, next) {
    console.log('i am error handling middleware', err);
    res.status(err.status || 400);
    res.json({
        message: err.msg || err,
        status: err.status || 400,
    });
});
emitter.on('err', function (err) {
    console.log('error on err events')
});

app.listen(config.port, function (err, done) {
    if (err) {
        console.log('error listening to port');
    }
    else {
        console.log('server listening at port ', config.port);
    }
});


// middleware
// middleware is a function that has access to 
// 1 http request object
// 2 http response object
// 3 refrence of next middleware function
// middleware are very important part of express 
//##############################IMPORTANT########################
// the order of middleware matter
//##############################IMPORTANT########################


// types of middlewares
// 1 application level middleware
// 2 routing level middleware
// 3 error hanlding middleware
// 4 third party middleware
// 5 inbuilt middleware

// syntax
// function(req, res, next) {
// middle ware function 
// req is http request object
// res http response object
// next next middleware function
// }

// app.use() // it is a configuration block to place middleware
// complete syntax of middleware
// app.use(function(req,res,next){

// })




// nodejs globals
// global 
// process
// __dirname
// __filename