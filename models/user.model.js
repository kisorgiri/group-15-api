const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    name: String,
    address: {
        type: String
    },
    username: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        sparse: true
    },
    phoneNumber: Number,
    dob: Date,
    gender: {
        type: String,
        enum: ['male', 'female', 'others']
    },
    role: {
        type: Number, //1 /2 admin/normal user
        default: 2
    },
    status: {
        type: Boolean,
        default: true
    },
    passwordResetExpiry: Date,
    passwordResetToken: String
}, {
        timestamps: true
    });
const UserModel = mongoose.model('user', UserSchema);
module.exports = UserModel;