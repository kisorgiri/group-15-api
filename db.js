// db connections

const mongoose = require('mongoose');
const { conxnURL, dbName } = require('./config/db.config');

let dbUrl;
if (process.env.MLAB) {
    dbUrl = 'mongodb://group16:group16@ds044979.mlab.com:44979/group16db'
} else {
    dbUrl = conxnURL + '/' + dbName;
}


mongoose.connect(dbUrl, { useNewUrlParser: true });
mongoose.connection.once('open', function (data) {
    console.log('db connection is now open');
});
mongoose.connection.on('error', function (err) {
    console.log('error connecting to db');
})