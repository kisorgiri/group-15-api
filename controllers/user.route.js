var router = require('express').Router();
var map_user = require("./../helpers/map_user_req");
const UserModel = require('./../models/user.model');
var passwordHash = require('password-hash');
const authorize = require('./../middlewares/authorize');
router.route('/')
    .get(function (req, res, next) {
        console.log('req.user >>>', req.user);

        //
        console.log('req.query >>', req.query);
        var condition = {};
        var page = Number(req.query.page) - 1 || 0
        var pageCount = Number(req.query.pageCount) || 10;
        var skipValue = page * pageCount;

        // console.log('skip value >>', skipValue);
        // console.log('page COunt =', pageCount);
        UserModel.find(condition)
            .sort({ _id: -1 })
            .skip(skipValue)
            .limit(pageCount)
            .exec(function (err, users) {
                if (err) {
                    return next(err);
                }
                res.status(200).json(users);
            })

    });



router.route('/change-password')
    .get(function (req, res, next) {
        var fs = require('fs');
        fs.readFile('sdklfj', function (err, done) {
            if (err) {
                return next(err);
            }
        })
    })
    .post(function (req, res, next) {
        console.log("post here");
        res.end('hi');
    })
    .put(function (req, res, next) {

    })
    .delete(function (req, res, next) {

    });
router.route('/:id')
    .get(function (req, res, next) {
        console.log('req.user >>>', req.user);

        // UserModel.findOne({ _id: req.params.id })
        //     .then(function (user) {
        //         res.status(200).json(user);
        //     })
        //     .catch(function (err) {
        //         next(err);
        //     })
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            res.status(200).json(user);
        })

    })
    .put(function (req, res, next) {
        console.log('req.user >>>', req.user);
        var id = req.params.id;
        UserModel.findById(id)
            .exec(function (err, user) {
                if (err) {
                    return next(err);
                }
                if (!user) {
                    return next({
                        msg: 'User not found'
                    })
                }
                // user is object which is instance of UserModel
                var updatedUser = map_user(user, req.body);
                if (req.body.password) {
                    updatedUser.password = passwordHash.generate(req.body.password);
                }
                updatedUser.updatedBy = req.user.name;
                updatedUser.save(function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(done);
                })
            })
    })
    .delete(authorize, function (req, res, next) {
        console.log('req.user >>>', req.user);

        // UserModel.findByIdAndRemove(req.params.id, function (err, user) {
        //     if (err) {
        //         return next(err);
        //     }
        //     res.status(200).json(user);
        // })
        UserModel.findById(req.params.id, function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                user.remove(function (err, done) {
                    if (err) {
                        return next(err);
                    }
                    res.status(200).json(done);
                })
            } else {
                next({
                    msg: "user not found"
                })
            }
        })
    })



module.exports = router;