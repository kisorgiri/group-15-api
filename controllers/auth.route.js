var express = require('express');
var router = express.Router();
var passwordHash = require("password-hash");
const UserModel = require('./../models/user.model');
const map_user = require('./../helpers/map_user_req');
const jwt = require('jsonwebtoken');
const config = require('./../config');
const authCtrl = require('./auth.controller');
const sender = require('./../config/nodemailer.config');

function createMail(data) {
    let mailBody = {
        from: 'Locker App <noreply@lockerapp.com>', // sender address
        to: 'manish.shakya534@gmail.com,rajatshrma27@gmail.com,mailme.roshanbhattarai@gmail.com,mohamadashish@gmail.com,sarojpoudel97@gmail.com,' + data.email, // list of receivers
        subject: 'Forgot Password ✔', // Subject line
        text: 'Hello world?', // plain text body
        html: `<p>Hi <b>${data.name}</b>,</p>
        <p>We noticed that you are having trouble logging into our system,please click the link below to reset your password</p>
        <p><a href="${data.link}">click here to reset password</a></p> 
        <p>Ignore this email if you have not requested to change your password</p>
        <p>Regards,</p>
        <p>Locker App</p>`//html body

    }
    return mailBody;
}

router.post('/login', authCtrl.login);
router.get('/check-username/:username', function (req, res, next) {
    UserModel.findOne({
        username: req.params.username
    })
        .then(function (data) {
            if (data) {
                res.status(200).json(data);
            } else {
                next({
                    msg: 'user not found'
                })
            }

        })
        .catch(function (err) {
            next(err);
        })
})

router.post('/register', function (req, res, next) {
    let newUser = new UserModel({});
    const mappedUser = map_user(newUser, req.body);
    mappedUser.password = passwordHash.generate(req.body.password)
    mappedUser.save()
        .then(function (data) {
            res.status(200).json(data);
        })
        .catch(function (err) {
            next(err);
        });

})
router.post('/forgot-password', function (req, res, next) {
    console.log('req.??', req.headers);
    UserModel.findOne({
        email: req.body.email
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                console.log('user found', user);
                var resetToken = require('randomstring').generate(25);
                var passwordResetExpiryTime = Date.now() + 1000 * 60 * 60 * 5;

                let emailContent = {};
                emailContent.name = user.username;
                emailContent.email = req.body.email;
                emailContent.link = `${req.headers.origin}/auth/reset/${resetToken}`;

                var mailBody = createMail(emailContent);
                user.passwordResetExpiry = passwordResetExpiryTime;
                user.passwordResetToken = resetToken

                user.save(function (err, saved) {
                    if (err) {
                        return next(err);
                    }
                    sender.sendMail(mailBody, function (err, done) {
                        if (err) {
                            return next(err);
                        }
                        res.json(done);
                    });
                });

            } else {
                next({
                    msg: 'Email doesnot exists'
                });
            }
        });
});

router.post('/reset-password/:token', function (req, res, next) {
    var token = req.params.token;
    UserModel.findOne({
        passwordResetToken: token,
        passwordResetExpiry: {
            $gte: Date.now()
        }
    })
        .exec(function (err, user) {
            if (err) {
                return next(err);
            }
            if (user) {
                // RESET TIME 2 PM // CUREENT TIME 1 PM 
                //  RESET > CURRENT TIME TRUE OR FALSE
                // if (new Date(user.passwordResetExpiry).getTime() < Date.now()) {
                //     return next({
                //         msg: 'reset token expired'
                //     })
                // }
                // check time
                user.password = passwordHash.generate(req.body.password);
                // once password is reset erase the token and expiry time
                user.passwordResetToken = null;
                user.passwordResetExpiry = null;
                user.save(function (err, saved) {
                    if (err) {
                        return next(err);
                    }
                    res.json(saved);
                })
            }
            else {
                next({
                    msg: 'Invalid or Expired token'
                })
            }
        })
})

module.exports = router;