// mongodb is nosql database 
// we wiill have collection and documents

// basic command
// mongo // it will start mongo shell

// shell command
// show dbs // list all the availabel database
// use <db_name> if Db name exist select exisitng db if not create new db and select it
// db // it will show selected db

// show collections
// it will list exisitng collection for db

// create collection with inserting
// db.<col_name>.insert({vlaid json});


// find document
// db.<col_name>.find({query_builder}) // list all the documents of collections
// db.<col_name>.find({}).pretty() // list all the documents of collections
// db.<col_name>.find({}).count() // list all the documents of collections
// eg  db.students.find({class:5})

// update document
// db.<col_name>.update({'query_builder'},{$set:{object_to_be_updated}},{otions,multi,upsert});
// object_to_be_updated==> if property exists replace the value if new property then upsert in the document


// remove
// db.<col_name>.remove({});

// drop collection
// db.<col_name>.drop()

// drop database
// use db_name
// db.dropDatabase()

// ############DB BACKUP AND RESTORE##################///
// two format of back and restore
// bson format (binary json)
// human readable format(csv,json)

// 1bson format
// backup command
// mongodump // it will backup all the database of system in default dump folder
// mongodump --db <db_name> it will backup selected db in default dump folder
// mongodump --db <db_name> --out <path_to_output_directory>
//======> mongodump will auto create folder if it doesnot exist


// restore command
// mongorestore this will restore all the data base that are inside default dump folder
// mongorestore <path_to_destination>
// mongodump and mongorestore always cames in PAIR

// with json and csv file
// json
// backup command
// mongoexport
// mongoexport --db <db_name> --collection <col_name> --out path_to_output_dir_with_.json extension
// mongoexport -d <db_name> -c <col_name> -o path_to_output_dir_with_.json extension

// restore 
// mongoimport --db <db_name> --collection <col_name> <path_to_destination_folder_containing_json_file>

// mongoimport and mongoexport always cames in pair

//csv format
// backup
mongoexport --db<db_name> --collection <col_name> --type=csv --fileds 'coma seperated filed name' --out <destinatino path with .csv extension>