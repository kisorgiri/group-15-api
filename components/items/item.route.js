const router = require('express').Router();
const ItemCtrl = require('./item.controller');
const authorize = require('./../../middlewares/authorize');


const multer = require('multer');
// var upload = multer({ dest: './uploads/' }); although it is a simple usuage it is not sufficient
var myStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/images/')
    },
    filename: function (req, file, cb) {
        console.log('file is >>', file);
        cb(null, Date.now() + '-' + file.originalname);
    }
});
function filter(req, file, cb) {
    var mimeType = file.mimetype.split('/')[0];
    if (mimeType != 'image') {
        req.fileError = true;
        cb(null, false)
    } else {
        cb(null, true)
    }
}
var upload = multer({
    storage: myStorage,
    fileFilter: filter
})


router.get('/', ItemCtrl.find);
router.post('/', upload.single('file'), ItemCtrl.insert);
router.route('/:id')
    .get(ItemCtrl.findById)
    .put(upload.single('file'), ItemCtrl.update)
    .delete(ItemCtrl.remove);

router.route('/search')
    .get(ItemCtrl.searchByGet)
    .post(ItemCtrl.searchByPost);


module.exports = router;